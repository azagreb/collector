#!/usr/bin/env python3
# IMPORT
from __init__ import *

# LOG
# log.setLevel('DEBUG')

# CONST
ID = 'interfaces_sw'
REQ_SECTION = '--- show running-config ---'

# VAR
result = []  # global list for a result of section parsing


# DEF
def get_interfaces_sw(req_section):
	"""
	Gets a section of 'show tech' file and parsing it to a dicts
	with keys 'Hostname', 'Interface Name', 'Description', 'State',
	'Port Mode', 'Access VLAN', 'Trunk VLANs', 'Native VLAN'
	:param req_section: list, by get_section()
	:return: list, list of dictionaries
	"""
	global result
	# making an object with all configuration
	all_parse = CiscoConfParse(req_section)
	# find a hostname
	hostname = all_parse.find_lines(r'^hostname')[0].split()[1]
	# find all intefaces that have IP addresses
	all_interface_with_ip = all_parse.find_objects_w_child('^interface ', 'switchport ')
	# then parse those interface one by one
	for iface in all_interface_with_ip:
		# create an ordered dictionary to put values in strict order
		single_interface = OrderedDict()
		single_interface['Hostname'] = hostname
		single_interface['Interface Name'] = iface.text.split()[1]
		# cut a block of interface configuration from a whole section
		iface_parse = all_parse.find_all_children(iface.text)

		# a few default values
		state, description, port_mode = 'Up', '-', '-'
		access_vlan, trunk_vlans, native_vlan = '-', '-', '1'

		# if specific line found as a child, put it in a variable
		for i in iface_parse:
			if 'shutdown' in i:
				state = 'Down'

			elif 'description' in i:
				description = i.split()[1]

			elif 'switchport mode ' in i:
				port_mode = i.split()[2]

			elif 'switchport access ' in i:
				access_vlan = int(i.split()[3])

			elif 'switchport trunk allowed vlan' in i:
				trunk_vlans = i.split()[4]

			elif 'switchport trunk native vlan' in i:
				native_vlan = i.split()[4]

		# add values to the dictionary in an order we want to
		single_interface['State'] = state
		single_interface['Description'] = description
		single_interface['Port Mode'] = port_mode
		single_interface['Access VLAN'] = access_vlan
		single_interface['Trunk VLANs'] = trunk_vlans
		single_interface['Native VLAN'] = int(native_vlan)

		# write the dict with interface info in the global list
		result.append(single_interface)
	return result


# MAIN
if __name__ == "__main__":
	top = CONF_DIR
	# walking through all directories in the directory CONF_DIR
	# (default is 'input')
	for current_root, dirs, files in walk(top):
		# do not work with the special directory 'ignore'
		if 'ignore' in dirs:
			dirs.remove('ignore')
		# in every directory except the top one
		if current_root is not top:
			current_dir = path.join(top, current_root)
			# iterate a list of files in current direcroty
			for f in files:
				# and if a filename has a proper extension
				if f.endswith(FILE_EXT):
					current_file = path.join(current_dir, f)
					# do the job
					# 1. cut off the section
					section = get_section(current_file, REQ_SECTION)
					# 2. parse it as required
					get_interfaces_sw(section)
			# and write the result where files lay
			to_excel(result, ID, path.join(current_dir, f'{ID}.xls'))
			# reset result as it is global
			result = []
