#!/usr/bin/env python3
# IMPORT
from __init__ import *

# LOG
# log.setLevel('DEBUG')

# CONST
ID = 'vlans'
REQ_SECTION = '--- show vlan ---'

# VAR
result = []  # global list for a result of section parsing


# DEF
def get_vlans(req_section):
	"""
	Gets a section of 'show tech' file and parsing it to a dicts
	with keys 'ID' and 'Name'.
	:param req_section: list, by get_section()
	:return: list, list of ordered dictionaries
	"""
	global result
	for item in req_section:
		# stop if line with system resersed vlans been reached
		if 'fddi-default' in item:
			break
		# search with regexp if it is a line with VLAN
		res = search(RE_VLAN, item)
		# if a match occured
		if res:
			# make a dict from values
			single_vlan = OrderedDict()
			single_vlan['ID'] = int(res.group(1))
			single_vlan['Name'] = res.group(2)
			# and add only new dicts to the global list - 'result'
			if single_vlan not in result:
				result.append(single_vlan)
	log.debug(result)
	return result


# MAIN
if __name__ == "__main__":
	top = CONF_DIR
	# walking through all directories in the directory CONF_DIR
	# (default is 'input')
	for current_root, dirs, files in walk(top):
		# do not work with the special directory 'ignore'
		if 'ignore' in dirs:
			dirs.remove('ignore')
		# in every directory except the top one
		if current_root is not top:
			current_dir = path.join(top, current_root)
			# iterate a list of files in current direcroty
			for f in files:
				# and if a filename has a proper extension
				if f.endswith(FILE_EXT):
					current_file = path.join(current_dir, f)
					# do the job
					# 1. cut off the section
					section = get_section(current_file, REQ_SECTION)
					# 2. parse it as required
					get_vlans(section)
			# and write the result where files lay
			to_excel(result, ID, path.join(current_dir, f'{ID}.xls'))
			# reset result as it is global
			result = []
