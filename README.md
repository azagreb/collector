# Collector 
#### Collector is a way to build Excel tables with a summary of information from 'show tech' outputs collected from networking devices at big amount of sites

Main ideas I've tried to follow while write this code:
+ to make it readable and understandable, so any person with basic understanding of Python could easily read and expand it.
+ provide a comment for an every action in code
+ separate 

#### Requirements:
+ python 3.6+
+ list of dependencies is in the file `requirements.txt`
+ install them with `sudo easy_install3 pip` && `sudo pip3 install -r requirements.txt`

### Usage:
+ manually gather 'show tech-support' outputs from all sites and devices you need to
+ group up outputs site by site in separate directories
+ place those directories in the top directory 'input' (see section Tree below) 
+ place sites you dont want to temporarily parse in 'input/ignore' as it is ignored by collector
+ run a python script of desired outcome, like this:
```text
host# ./cdp-graph.py
2018-02-28 01:43:19,399 | INFO | + ~\collector\input\example-site1\cdp-graph.png
2018-02-28 01:43:19,830 | INFO | + ~\collector\input\example-site2\cdp-graph.png
```
+ an outcome file will be created in a directory of every site

#### Current list of outcomes:
+ **vlans.xls** - a table with columns: ID, VLAN Name
+ **versions.xls** - a table with columns: Hostname, Version
+ **interfaces_ip.xls** - a table with columns: Hostname, Interface Name, State, Description, IP Address, Subnet, VRF Name, Have secondary IPs
+ **interfaces_sw.xls** - a table with columns: Hostname, Interface Name, State, Description, Port Mode, Access VLAN, Trunk VLANs, Native VLAN
+ **cdp-graph.png** -  an image of graph forming by CDP neighbors

#### Tree

```text
.
|-- input
|   |-- ignore
|   |   `-- site0
|   |       `-- everything_here_is_ignored
|   |
|   |-- example-site1
|   |   |-- deviceQ_show_tech.log
|   |   `-- deviceW_show_tech.log
|   |
|   |-- example-site2
|       |-- deviceX_show_tech.log
|       `-- deviceY_show_tech.log
|
|-- __init__.py
|-- versions.py
|-- vlans.py
|-- interfaces_ip.py
|-- interfaces_sw.py
|-- cdp-graph.py
|-- requirements.txt
|-- LICENSE
`-- README.md
```

###### TODO
+ put in a Docker container