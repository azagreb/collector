# IMPORT
from re import compile, search
from os import getcwd, listdir, chdir, path, walk
from logging import basicConfig, getLogger
from pyexcel import save_book_as
from ciscoconfparse import CiscoConfParse
from collections import OrderedDict

# CONST
ROOT_DIR = getcwd()
CONF_DIR = path.join(ROOT_DIR, 'input')
FILE_EXT = '.log'

# RE
RE_ANY_SECTION = compile(r"^------------------[\s\w-]{10,}------------------")
RE_VLAN = compile(r"^(\d+)\s+([\S\d]+)")  # g1 - id, g2 - name
RE_VERSION = compile(r", Version ([\d\S.]+),")  # g1 - version
RE_UPTIME = compile(r"([\d\S]+) uptime is (.+)")  # g1 - hostname, g2 - uptime

# RE from 'show running-config'
RE_CONF_PAD = compile(r"^!$")
RE_CONF_VERSION = compile(r"^version\s(.+)$")
RE_CONF_HOSTNAME = compile(r"hostname\s(.+)$")
RE_CONF_INTERFACE = compile(r"^interface (.+)")  # g1 - interface name
RE_CONF_DESC = compile(r"\sdescription (.+)")  # g1 - description
RE_CONF_STATE = compile(r"\s(shutdown)")  # g1 - 'shutdown'
RE_CONF_IP = compile(r"\sip address ([\d\.]{3,15}) ([\d\.]{3,15})")  # g1 - ip, g2 - mask

# LOG
basicConfig(format='%(asctime)s | %(levelname)s | %(message)s', level='INFO')
log = getLogger(__name__)


# DEF
def get_section(filename, req_section):
	"""
	Cutting a section from 'show tech-support' file
	:param filename: str, full path to a file with 'show tech' output
	:param req_section: str, section name how it appears in 'show tech' file
	:return: list, result where an every line of input file, becomes an item
	"""
	section = []
	# iterate through all 'show tech' files in a directory
	with open(filename, 'r') as f:
		section_start_reached = False
		# section_end_reached = False
		# check every line if it matches to required patterns
		for line in f:
			# if we are in state 'reached' and other section appears - stop parsing
			if section_start_reached:
				if search(RE_ANY_SECTION, line):
					# section_end_reached = True
					break
				# add a line of section to the list 'section'
				section.append(line)
			# if found a required section name then go to state 'reached'
			if req_section in line:
				section_start_reached = True
	log.debug(f' > {filename} | Collected section {req_section}')
	return section


def to_excel(result_list, sheet_name, destfile):
	"""
	Write an Excel file with a result of gathering
	:param result_list: list, a list of dictionaries
	:param sheet_name: str, name of a sheet to place a result
	:param destfile: str, name of a file where resulting sheet must be inserted
	:return: None
	"""
	# if resulting list isn't empty
	if result_list:
		try:
			# try to write it in an excel file
			save_book_as(records=result_list, sheet_name=sheet_name, dest_file_name=destfile)
		# or raise a show explanation why it is not possible
		except Exception as e:
			log.warning(e.__doc__)
		else:
			log.info(f'+ ~ {destfile}')
	else:
		log.info(f'- ~ {destfile}')
	#
	return None
