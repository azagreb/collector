#!/usr/bin/env python3
# IMPORT
from __init__ import *
import networkx as nx
import matplotlib

matplotlib.use('Agg')  # matplotlib special fix
import matplotlib.pylab as plot

# LOG
# log.setLevel('DEBUG')

# CONST
ID = 'cdp-graph'
REQ_SECTION = '--- show cdp neighbor detail ---'
REQ_ALT_SECTION = '--- show cdp neighbors detail ---'  # neighborS
CONF_SECTION = '--- show running-config ---'

# VAR
result = []  # global list for a result of section parsing


# DEF
def get_head_node(req_section):
	"""
	Getting a hostname by the separate function,
	as it does not reside in a section of CDP
	:param req_section: list, gathered by get_section()
	:return: str, hostname from a current 'show tech' file
	"""
	for item in req_section:
		# finding a hostname string
		res = search(RE_CONF_HOSTNAME, item)
		if res:
			# return the found hostname
			return res.group(1)


def get_links(graph_obj, head_node, req_section):
	"""
	Parsing CDP section and add an edge between
	the 'head_node' and a found 'tail_node'
	to graph graph_obj
	:param graph_obj: object of nx.Graph()
	:param head_node: str, head_node of an edge, by get_hostname()
	:param req_section: list, by get_section()
	:return: None
	"""
	for item in req_section:
		if 'Device ID: ' in item:
			graph_obj.add_edge(head_node, str(item.split()[2]))
	return None


# MAIN
if __name__ == "__main__":
	top = CONF_DIR
	# walking through all directories in the directory CONF_DIR
	# (default is 'input')
	for current_root, dirs, files in walk(top):
		# do not work with the special directory 'ignore'
		if 'ignore' in dirs:
			dirs.remove('ignore')
		# in every directory except the top one
		if current_root is not top:
			current_dir = path.join(top, current_root)
			# iterate a list of files in current directory
			G = nx.Graph()
			for f in files:
				# and if a filename has a proper extension
				if f.endswith(FILE_EXT):
					current_file = path.join(current_dir, f)
					# do the job
					# 0. pre-fetch a hostname
					hostname_section = get_section(current_file, CONF_SECTION)
					head = get_head_node(hostname_section)
					# 1. fetch the section
					section = get_section(current_file, REQ_SECTION)
					if not section:
						section = get_section(current_file, REQ_ALT_SECTION)
					# 2. parse it as required
					get_links(G, head, section)
					log.debug(f'NODES: {G.nodes()}')
					log.debug(f'LINKS: {G.edges()}')
			# trying to save a graph
			current_graph = path.join(current_dir, f'{ID}.png')
			if G.edges():
				plot.figure(figsize=(10, 6))
				nx.draw_networkx(G, node_size=900, edge_color='blue', node_color='red', with_labels=True)
				plot.axis('off')
				plot.title(f'{path.basename(current_root)} diagram', size=10)
				plot.savefig(current_graph, dpi=240, bbox_inches='tight')
				# plot.show()
				# reset a graph
				plot.clf()
				log.info(f'+ ~ {current_graph}')
			else:
				log.info(f'- ~ {current_graph}')
