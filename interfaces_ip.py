#!/usr/bin/env python3
# IMPORT
from __init__ import *
from netaddr import IPNetwork

# LOG
# log.setLevel('DEBUG')

# CONST
ID = 'interfaces_ip'
REQ_SECTION = '--- show running-config ---'

# VAR
result = []  # global list for a result of section parsing


# DEF
def get_interfaces_ip(req_section):
	"""
	Gets a section of 'show tech' file and parsing it to a dicts
	with keys 'Hostname', 'Interface Name', 'Description', 'State',
	'IP Address', 'Subnet', 'VRF Name
	:param req_section: list, by get_section()
	:return: list, list of ordered dictionaries
	"""
	global result
	# making an object with all configuration
	all_parse = CiscoConfParse(req_section)
	# find a hostname
	hostname = all_parse.find_lines(r'^hostname')[0].split()[1]
	# find all intefaces that have IP addresses
	all_interface_with_ip = all_parse.find_objects_w_child('^interface ', 'ip address ')
	# then parse those interface one by one
	for iface in all_interface_with_ip:
		# create an ordered dictionary to put values in strict order
		single_interface = OrderedDict()
		single_interface['Hostname'] = hostname
		single_interface['Interface Name'] = iface.text.split()[1]
		# cut a block of interface configuration from a whole section
		iface_parse = all_parse.find_all_children(iface.text)

		# a few default values
		state, description, vrf_name, ip_address, ip_subnet, have_secondary = 'Up', '-', '-', '-', '-', '-'

		# if specific line found as a child, put it in a variable
		for i in iface_parse:
			if 'shutdown' in i:
				state = 'Down'

			if 'description' in i:
				description = i.split()[1]

			if 'ip vrf forwarding ' in i:
				vrf_name = i.split()[3]

			if 'ip address ' in i:
				if 'dhcp' in i:
					ip_address = 'DHCP'
				else:
					ip = i.split()[2]
					mask = i.split()[3]
					addr = IPNetwork(f'{ip}/{mask}')
					ip_address = ip
					ip_subnet = addr.cidr.__str__()
					if 'secondary' in i:
						have_secondary = 'Yes'

		# add values to the dictionary in an order we want to
		single_interface['State'] = state
		single_interface['Description'] = description
		single_interface['IP Address'] = ip_address
		single_interface['Subnet'] = ip_subnet
		single_interface['VRF Name'] = vrf_name
		single_interface['Have secondary IPs'] = have_secondary

		# write the dict with interface info in the global list
		result.append(single_interface)
	return result


# MAIN
if __name__ == "__main__":
	top = CONF_DIR
	# walking through all directories in the directory CONF_DIR
	# (default is 'input')
	for current_root, dirs, files in walk(top):
		# do not work with the special directory 'ignore'
		if 'ignore' in dirs:
			dirs.remove('ignore')
		# in every directory except the top one
		if current_root is not top:
			current_dir = path.join(top, current_root)
			# iterate a list of files in current direcroty
			for f in files:
				# and if a filename has a proper extension
				if f.endswith(FILE_EXT):
					current_file = path.join(current_dir, f)
					# do the job
					# 1. cut off the section
					section = get_section(current_file, REQ_SECTION)
					# 2. parse it as required
					get_interfaces_ip(section)
			# and write the result where files lay
			to_excel(result, ID, path.join(current_dir, f'{ID}.xls'))
			# reset result as it is global
			result = []
