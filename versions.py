#!/usr/bin/env python3
# IMPORT
from __init__ import *

# LOG
# log.setLevel('DEBUG')

# CONST
REQ_SECTION = '--- show version ---'
ID = 'versions'

# VAR
result = []  # global list for a result of section parsing


# DEF
def get_version(req_section):
	"""
	Gets a section of 'show tech' file and parsing it to a dicts
	with keys 'Hostname' and 'Version'.
	:param req_section: list, by get_section()
	:return: list, list of ordered dictionaries
	"""
	global result
	hostname, version = '', ''
	# parse line by line with regexps
	for item in req_section:
		res1 = search(RE_UPTIME, item)
		res2 = search(RE_VERSION, item)
		# if hostname found then write to a variable
		if res1:
			hostname = res1.group(1)
		# if version found then write to a variable
		if res2:
			version = res2.group(1)
		# if len(hostname) > 0 and len(version) > 0:
		if hostname and version:
			single_version = OrderedDict()
			single_version['Hostname'] = hostname
			single_version['Version'] = version
			hostname, version = '', ''
			# add only new items
			if single_version not in result:
				result.append(single_version)
	return result


# MAIN
if __name__ == "__main__":
	top = CONF_DIR
	# walking through all directories in the directory CONF_DIR
	# (default is 'input')
	for current_root, dirs, files in walk(top):
		# do not work with the special directory 'ignore'
		if 'ignore' in dirs:
			dirs.remove('ignore')
		# in every directory except the top one
		if current_root is not top:
			current_dir = path.join(top, current_root)
			# iterate a list of files in current direcroty
			for f in files:
				# and if a filename has a proper extension
				if f.endswith(FILE_EXT):
					current_file = path.join(current_dir, f)
					# do the job
					# 1. cut off the section
					section = get_section(current_file, REQ_SECTION)
					# 2. parse it as required
					get_version(section)
			# and write the result where files lay
			to_excel(result, ID, path.join(current_dir, f'{ID}.xls'))
			# reset result as it is global
			result = []
